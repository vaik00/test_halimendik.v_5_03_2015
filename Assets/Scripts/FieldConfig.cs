﻿using UnityEngine;
using System.Collections;

public class FieldConfig : MonoBehaviour {
	public int width = 8;
	public int height = 8;

	public static FieldConfig Width { get; private set; }
	public static FieldConfig Height { get; private set; }

	void Awake() {
		CheckFieldSize ();
		Width = this;
		Height = this;
	}
	// Use this for initialization
	void Start () {
	
	}

	void CheckFieldSize() {
		if (width > 8)
			width = 8;
		if (height > 8)
			height = 8;
	}


	
	
	// Update is called once per frame
	void Update () {
	
	}
}
