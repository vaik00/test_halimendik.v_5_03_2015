﻿using UnityEngine;
using System.Collections;

public class FieldGenerator : MonoBehaviour {
	public Texture2D background;
	public Texture2D strawberry;
	public Texture2D grape;
	public Texture2D plum;
	public Texture2D mandarine;
	private Texture2D[,] fruitsMatrix;

	// Use this for initialization
	void Start () {
		fruitsMatrix = CreateFruitsMatrix ();	
	}

	void OnGUI() {
		GUI.Label (new Rect(0f, 0f, 757f, 650f), background);
		float y = 0f;
		for (int i = 0; i < fruitsMatrix.GetLength(0); i++, y += 78f) {
			float x = 0f;
			for (int j = 0; j < fruitsMatrix.GetLength(1); j++, x += 95f) {
				GUI.Label (new Rect(x, y, 95f, 83f), fruitsMatrix[i,j]);
			}
		} 
	}

	Texture2D[,] CreateFruitsMatrix() {
		Texture2D[,] fruitsMatrix = new Texture2D[FieldConfig.Height.height, FieldConfig.Width.width];
		for (int i = 0; i < fruitsMatrix.GetLength(0); i++) {
			for (int j = 0; j < fruitsMatrix.GetLength(1); j++) {
				fruitsMatrix[i, j] = GetSomeFruit();
				if (j > 1) {
					if (fruitsMatrix[i, j] == fruitsMatrix[i, j-1] == fruitsMatrix[i, j-2]) {
						fruitsMatrix[i, j] = GetSomeFruit();
						j--;
					}
				}
				if (i > 1) {
					if (fruitsMatrix[i, j] == fruitsMatrix[i - 1, j] == fruitsMatrix[i - 2, j]) {
						fruitsMatrix[i, j] = GetSomeFruit();
						if (j > 0)
							j--;
						else {
							i--;
							j = fruitsMatrix.GetLength(1);
						}
					}
				}
			}
		}
		return fruitsMatrix;
	}


	Texture2D GetSomeFruit() {
		switch (Random.Range(1, 5)) {
		case 1:
			return strawberry;
		case 2:
			return grape;
		case 3:
			return plum;
		case 4:
			return mandarine;
		default:
			return null;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
